import React from 'react'
import './footer.css'
import ImgLogo from '../../../assets/img/logo.jpg'

const Footer = () => (
    <footer>
        <div className="copyRight"> 
        <label>Dev. FullStack Amorim </label>
            <img src={ImgLogo} alt="" />
        </div>
    </footer>

)

export default Footer