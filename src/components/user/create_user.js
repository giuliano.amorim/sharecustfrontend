//Libs
import React, { useState, useEffect } from 'react'
import { useHistory, useParams} from 'react-router-dom'
import { createUser, showUserId, updateUser } from '../../services/user'

//Routes
// import Loading from '../loading/loading'
import Alert from '../alert/index'
import Nav from '../layout/nav/nav'

//Style
import './user.css'
import { Button, FormGroup, Label, Input, CustomInput } from "reactstrap";

//Auth
import jwt from "jsonwebtoken";
import { getToken } from "../../config/auth";



const UserCreate = (props) => {

    const [userIsAdmin, setUserIsAdmin] = useState({});

    const [isSubmit, setIsSubmit] = useState(false)

    const [isEdit, setIsEdit] = useState(false)

    const [alert, setAlert] = useState({})

    const history = useHistory()

    const { id } = useParams()

    const methodUser = isEdit ? updateUser : createUser

    const [form, setForm] = useState({
          is_admin: false
      });

    useEffect(() => {
        (async () => {
            const { user } = await jwt.decode(getToken());
            setUserIsAdmin(user.is_admin);
        })();
        return () => {};
        }, []);
    
 
    useEffect(() => {
        const getShowUser = async () => {
            const user = await showUserId(id)
            if (user.data.password) {
                delete user.data.password
            }
            setForm(user.data)
        }


        if (id) {
            setIsEdit(true)
            getShowUser()
        }
    }, [id])


        const handleChange = (event) => {
          const value =   
            event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value;
        const name = event.target.name;

        setForm({
        ...form,
        [name]: value,
        });
    };

      const formIsValid = () => {
          return form.name && form.cnpj && form.email && form.colaborar && form.exigir && form.password
      };

      const submitForm = async (event) => {
          try {

              setIsSubmit (true)
              await methodUser(form)
              const is_admin = userIsAdmin ? form.is_admin : false;
              setForm({
                ...form,
                is_admin,
              });

              setAlert({
                  type: "success",
                  message: 'Cadastro enviado com sucesso.',
                  show: true,
                });
              setIsSubmit(false);
              
              setTimeout(() => history.push("/"), 3000);
            } catch (e) {
            setAlert({
                type: "error",
                message: "Ocorreu um erro no cadastro",
                show: true,
            });
            setIsSubmit(false);
        }
      };
    
    return (
    <React.Fragment>
        <Nav name="Voltar" to="/" />
        <section>
            <Alert type={alert.type || ""} 
            message={alert.message || ""} 
            show={alert.show || false} />
            
            <div className="create_user">
                <div className="form_login">
                 <FormGroup>
                     <Label for="auth_name">Patrocinador:</Label>
                     <input 
                     disabled={isSubmit} 
                     type="text" id="auth_name" name="name" 
                     onChange={handleChange} value={form.name || ""} 
                     placeholder="Insira a sua razão social." />
                 </FormGroup>

                <FormGroup>
                    <Label for="auth_cnpj">CNPJ:</Label>
                    <Input disabled={isSubmit} type="number" id="auth_cnpj" 
                    name="cnpj" onChange={handleChange} value={form.cnpj || ""} 
                    placeholder="Insira o seu CNPJ (*Digite somente números*)" />
                </FormGroup>

                 <FormGroup>
                    <Label for="auth_email">Email:</Label>
                    <Input disabled={isSubmit || isEdit} type="email" 
                    id="auth_email" name="email" onChange={handleChange} 
                    value={form.email || ""} placeholder="Insira o seu email" />
                </FormGroup>

                <FormGroup>
                    <Label for="auth_colaborar">Proposta de colaboração:</Label>
                    <Input disabled={isSubmit} type="text" id="auth_colaborar" 
                    name="colaborar" onChange={handleChange} 
                    value={form.colaborar || ""} 
                    placeholder="Insira a sua ideia de colaboração" />
                </FormGroup>

                <FormGroup>
                    <Label for="auth_exigir">Exigência:</Label>
                    <input disabled={isSubmit} type="text" id="auth_exigir" 
                    name="exigir" onChange={handleChange} 
                    value={form.exigir || ""} 
                    placeholder="Insira a sua condição de exigência" />
                </FormGroup>

                <FormGroup>
                    <Label htmlFor="auth_password">Senha:</Label>
                    <Input disabled={isSubmit} type="password" id="auth_password" 
                    name="password" onChange={handleChange} value={form.password || ""} 
                    placeholder={isEdit ? `Atualize sua senha (Mínimo 06 letras ou números)` : 'Informe sua senha (Mínimo 06 letras ou números)'} />         
                </FormGroup>

                <FormGroup>
              {userIsAdmin ? (
                <CustomInput
                  type="switch"
                  label="Ativar Usuário como administrador:"
                  name="is_admin"
                  id="is_admin"
                  onChange={handleChange}
                  checked={(!isEdit ? true : form.is_admin) || false}
                />
              ) : (
                ""
              )}
              <CustomInput
                type="switch"
                label="Usuário ativo ?"
                id="is_active"
                name="is_active"
                onChange={handleChange}
                checked={(!isEdit ? true : form.is_active) || false}
              />
            </FormGroup>

            <Button
              color="primary"
              disabled={!formIsValid()}
              onClick={submitForm}
            >
              {isEdit ? "Atualizar" : "Cadastrar"}
            </Button>
          </div>
          <br />
          {isSubmit ? <div>Aguarde um momento....</div> : ""}
        </div>

      </section>
    </React.Fragment>
  );
};

export default UserCreate;