import React, {useEffect, useState} from 'react'

import {DeleteUser, ListUser} from '../../services/user'
import Loading from '../loading/loading'
import Nav from '../../components/layout/nav/nav'


import { Table, Button, Modal, ModalBody, ModalFooter } from 'reactstrap';


const List_user = (props) => {

  const [users, setUsers] = useState([])

  const[loading, setloading] = useState(false)

  const [isUpdate, setIsUpdate] = useState(false)

  const[confirmation, setConfirmation] = useState ({
    isShow: false,
    params: {}
  })



  //EDIT
  const editUser = (user) => props.history.push(`/edit/${user._id}`)
  
  //DELETE
  //Verificar se o DELETE vai estar operante
  const deleteUser = async () => {

    if (confirmation.params) {
        await DeleteUser(confirmation.params._id)
    }
    setConfirmation({
        isShow: false,
        params: {}
    })
    setIsUpdate(true)
}

const Confirmation = () => {
    const toggle = () => setConfirmation(!confirmation.isShow);
    return (
        <Modal isOpen={confirmation.isShow} toggle={toggle} className="info">
            <ModalBody>
                Você deseja excluir o usuário {(confirmation.params && confirmation.params.name) || ""}
            </ModalBody>
            <ModalFooter>
                <Button color="success" onClick={deleteUser}>SIM</Button>{' '}
                <Button color="danger" onClick={toggle}>NÃO</Button>
            </ModalFooter>
        </Modal>
    )
}


  const verifyIsEmpty = users.length === 0

  const sortList = (users) => {
    return users.sort((a, b) => {
        if (a.is_active < b.is_active) {
            return 1;
        }
        if (a.is_active > b.is_active) {
            return -1;
        }
        return 0;
    })
  }

  const setIcon = (conditional) => (
    <i className={`action fa fa-${conditional ? "check text-success" : "times text-danger"}`} />
)

  const mountTable = () => {
    const listSorted = sortList(users)

    const rows = listSorted.map((user, index) => (
    <tr key={index} className={user.is_active ? "" : "table-danger"} >
      <td>{setIcon(user.is_active)}</td>
      <td>{setIcon(user.is_admin)}</td>
      <td>{user.name}</td>
      <td>{user.email}</td>
      <td>
      <span onClick={() => editUser(user)} className="text-primary mx-1" style={{cursor: "pointer"}}  >
            <i className="action fa fa-edit"></i>
        </span>
        <span onClick={() => setConfirmation({ isShow: true, params: user })} className="text-danger  mx-1" style={{cursor: "pointer"}} >
            <i className="action fa fa-trash"></i>
        </span>
      </td>
    </tr>
  ))

  
//render() //
  return ( !verifyIsEmpty ? (
      <Table className="table table-striped table-dark">
        <thead className="thead-dark">
              <tr>
                <th>ATIVO</th>
                <th>ADMIN</th>
                <th>PATROCINADORES</th>
                <th>EMAIL</th>
                <th>AÇÕES</th>
              </tr>
        </thead>
            <tbody>
                {rows}
            </tbody>
      </Table>
  ) : ""
  )}


// More perfomance
  const fncUseEffect = () => {
    let isCancelled = false;
    (async () => {
        try {
            setloading(true)
            const usersAll = await ListUser()
            if (!isCancelled) {
                if (usersAll) {
                    setUsers(usersAll.data)
                }
                setloading(false)
                setIsUpdate(false)
            }
        } catch (error) {
            setloading(false)
        }
    })()

    return () => { isCancelled = true }
}

// INIT 
useEffect(fncUseEffect, [])

// UPDATE 
useEffect(fncUseEffect, [isUpdate])




// Less perfomance
// useEffect(() => {
//   getListUser()
// }, [])



  return (
    <div>

        <Nav name="Novo Cadastro" to="/create_user" />
          <Confirmation />

        <section>

          <div className="list_user">
            <Loading show={loading} />
            {mountTable()}
          </div>

        </section>

    </div>

    )
}


export default List_user