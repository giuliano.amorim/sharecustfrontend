//Libs
import React from 'react'
import {
    Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";


import history from './config/history';
import ErrorHandler from './views/errors/error';
import viewUser from './views/User';
import viewLogin from './views/Login';

import { isAuthenticated } from './config/auth';


const CustomRoute = ({ ...rest }) => {
    if (!isAuthenticated()) {
        return <Redirect to='/login' />
    }
    return <Route  {...rest} />
}

const Routers = () => (
    <Router history={history}>
        <Switch>
            <Route exact path="/login" component={viewLogin} />
            <Route exact path="/erro/:erro" component={ErrorHandler} />
            <CustomRoute path="/" component={viewUser} />


        </Switch>
    </Router>
)

export default Routers;
