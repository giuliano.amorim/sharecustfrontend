import { clientHttp } from '../config/config'

const createUser = (data) => clientHttp.post(`/user`, data)


const ListUser = () => clientHttp.get(`/user`)


const DeleteUser = (id) => clientHttp.delete(`/user/${id}`)



const showUserId = (id) => clientHttp.patch(`/user/${id}`)


const updateUser = (data) => clientHttp.patch(`/user/${data._id}`, data)




export {
    createUser,
    ListUser,
    DeleteUser,
    showUserId,
    updateUser
}