## Versão

0.1.0

## Pré Requisitos
* node

## Instalação

''' npm install '''

## Root API:

/api/

##  Link para o repositório do projeto no gitLab:

 https://gitlab.com/giuliano.amorim/sharecust

 ## Link para o projeto no Trello:

 https://trello.com/b/0UAr1fgp/projetoindividual01-sharecust


 ## Tecnologias utilizadas:

 NodeJs
 React
 MongoDB
 Express
 Bcryptjs
 Cors
 Jwt

## Deploy:
https://sharecustfrontend.vercel.app/